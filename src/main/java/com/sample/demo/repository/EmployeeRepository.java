package com.sample.demo.repository;

import com.sample.demo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    void deleteEmployeeByEmployeeId(Integer employeeId);
    Employee findEmployeeByEmployeeId(Integer employeeId);
}
