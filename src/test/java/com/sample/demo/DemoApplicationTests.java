package com.sample.demo;

import com.sample.demo.model.Employee;
import com.sample.demo.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(value = false)
class DemoApplicationTests {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    public void getEmployeeList(){
        List<Employee> employees = employeeRepository.findAll();
        assert (!employees.isEmpty());
    }
    @Test
    public void addNewEmployee(){
        Employee employee = new Employee();
        employee.setAge(18);
        employee.setFullname("Nguyen Van K");
        employee.setGender('f');
        Employee e = employeeRepository.save(employee);
        assert (e!=null);
    }
    @Test
    public void deleteEmployee(){
        employeeRepository.deleteEmployeeByEmployeeId(1);
        assert (!employeeRepository.existsById(1));
    }
    @Test
    public void updateEmployee(){
        Employee e = employeeRepository.findEmployeeByEmployeeId(1);
        e.setAge(20);
        employeeRepository.save(e);
        assert (e!= null);
    }

}
